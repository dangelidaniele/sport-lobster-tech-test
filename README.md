Daniele D'Angeli feed reader
========================

1) Installing the Feed Reader
----------------------------------

After having fetched the repository through Git or unzipped the content in your local folder, then:

### Get Composer
Run the following command:

    curl -s http://getcomposer.org/installer | php

### Setting up the cache
Following the Symfony instructions to set the cache permissions

Then update the dependencies

    php composer.phar install

###Assets install

    php app/console assets:install web --symlink

2) Browsing the Application
--------------------------------

To see the site in action go to:

    web/app_dev.php/football
    web/app_dev.php/football/Report

3) Test
-------------------------------

The project is tested with phpunit.
If you have phpunit installed in your machine
run the following command:

    phpunit -c app/

Considerations and Application Structure
---------------

###Application Structure

The application is divided in two bundles:

  * [**SportLobsterFeedBundle**]- That contains the low level logic of application
  * [**SportLobsterTestCoreBundle**] - That contains the controller and a simple service that uses the **SportLobsterFeedBundle**
  to perform the use cases (football and football/{category} routes)

###Configuration

The application might be configured in order to add different sources (now there is only the sky football channel, but
in the future new sources can be added with different attributes).
To configure the sources, have a look to config.yml (or config_dev.yml) under the 'sport_lobster_feed:' section:

    sport_lobster_feed:
        channels_sources:
            sky_sport_football:
                url: http://www.skysports.com/rss/0,20514,11959,00.xml
                strategy: xml
                category: football
                ttl: 120
                exclusion_categories:
                    - Preview
where:

  * **sky_sport_football**: is the name of the channel
  * **url**: is the endpoint url
  * **strategy**: is the format endpoint (xml or json, as per specifications)
  * **category**: is the category of the channel
  * **ttl**: is the max cache time of the channel (see Cache paragraph) | set to 0 if you don't want to cache the data
  * **exclusion_categories**: is a list of feed category that won't be showed (as per specifications for
             'Preview' category). More than one category can be removed from the channel preview, if needed.

For the test purpose I decided to add this configuration in the config file. It might be better to
keep this configuration data in a database layer (when the number of the channels increases).

The configuration file is passed to SportLobsterFeedBundle extension class ( see http://symfony.com/doc/current/cookbook/bundles/extension.html )

    $container->setParameter('sport_lobster_feed.channels.config', $config);

Then the config info is maintained by **SourceFeedManager** and an array of **ChannelInfo** is built up.

The **ChannelInfo** is a domain object that will be used by the **FeedManager** to retrieve the right strategy
to fetching the data (that can be Json or Xml). To get this strategy, there is a very little
strategy pattern that will return an object of **JsonStrategyFetcher** or **XmlStrategyFetcher**.
Both this classes implement the **FetcherStrategyInterface** and the method *fetch*.

Then **FeedManager** calls the method fetch() to get the channel data.
At this point a *Rss* model is built (by using the Jms Serializer) that contains the *Channel* information.
When the data is hydrated over our domain classes, the manager filter the feeds, by removing the ones that are in the
exclusion_categories, and will return the filtered channel.

###Cache consideration
The application caches the data retrieved from the channel by using the Guzzle Cache Plugin
(see http://guzzle3.readthedocs.org/en/latest/plugins/cache-plugin.html)
Following the **XmlStrategyFetcher** fetch method:

    public function fetch()
        {
            $request = $this->client->get($this->channelInfo->getUrl());

            if($this->channelInfo->isCacheEnabled()) {
                $request->getParams()->set('cache.override_ttl', $this->channelInfo->getTtl());
            }

            $data = $request->send()->getBody(true);

            return $data;
        }

If the **ChannelInfo** has a ttl > 0 then the cache is enabled and the channel info ttl is setted as lifetime.

I though that a
simple caching strategy is essential for this kind of tasks a in real world use.
The RSS feed has a ttl parameter because this kind of information changes after a period of time and we can
take advantage about that by caching the information.
Obviously others caching strategy can be used. This one is a low level strategy (HTTP level) but for example, by using
some services like Redis, the hydrated data (Rss, Channel and Feed) can be cached and maintain


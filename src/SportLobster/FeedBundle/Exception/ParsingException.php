<?php

namespace SportLobster\FeedBundle\Exception;

use SportLobster\FeedBundle\Exception;

class ParsingException
    extends \Exception
    implements Exception
{}
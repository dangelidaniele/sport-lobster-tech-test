<?php

namespace SportLobster\FeedBundle\Exception;

use SportLobster\FeedBundle\Exception;

class SourceChannelNotFoundException
    extends \Exception
    implements Exception
{}
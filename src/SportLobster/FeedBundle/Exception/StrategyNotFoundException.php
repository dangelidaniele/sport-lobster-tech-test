<?php

namespace SportLobster\FeedBundle\Exception;

use SportLobster\FeedBundle\Exception;

class StrategyNotFoundException
    extends \Exception
    implements Exception
{}
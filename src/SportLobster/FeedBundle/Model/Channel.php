<?php

namespace SportLobster\FeedBundle\Model;

/**
 * Class Channel
 * @package SportLobster\FeedBundle\Model
 * <title>Sky Sports | Football </title>
<link>http://www.skysports.com</link>
<description>Europa League News</description>
<language>en-gb</language>
<lastBuildDate>Sat, 26 Apr 2014 15:20:18 GMT</lastBuildDate>
<copyright>Copyright 2014, BSKYB. All Rights Reserved.</copyright>
<category>Football</category>
<image>
<title>Sky Sports</title>
<url>http://www.skysports.com/Images/skysports/site/ss-logo-07.gif</url>
<link>http://www.skysports.com</link>
</image>
<ttl>120</ttl>

 */
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;

/**
 * Class Channel
 * @package SportLobster\FeedBundle\Model
 */

class Channel
{
    /**
     * @var  string
     * @JMS\Type("string")
     */
    public $title;

    /** @var  string
     *  @JMS\Type("string")
     */
    public $link;

    /**
     * @var  string
     * @JMS\Type("string")
     */
    protected $language;

    /**
     * @var  string
     * @JMS\Type("string")
     */
    protected $category;

    /**
     * @var  string
     * @JMS\Type("string")
     */
    protected $image;

    /**
     * @var  ArrayCollection
     * @JMS\Type("ArrayCollection<SportLobster\FeedBundle\Model\Feed>")
     * @JMS\XmlList(inline = true, entry="item")
     */
    protected $item;

    /**
     * @param string $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * @return Feed[]
     */
    public function getFeeds()
    {
        return $this->item;
    }

    public function addItem($item)
    {
        $this->item = $item;
    }

    public function removeFeed(Feed $feed)
    {
        $this->item->removeElement($feed);
    }
} 
<?php

namespace SportLobster\FeedBundle\Model;

/**
 * Class ChannelInfo
 * @package SportLobster\FeedBundle\Model
 *
 * A channel info is used to retrieve
 * the information of a channel feed.
 * The strategy attribute is used to fetch the right
 * strategy Feed
 */
class ChannelInfo
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     * http link feed url
     */
    protected $url;

    /**
     * @var string
     * http category
     */
    protected $category;

    /**
     * @var string
     * //could be 'json' or 'xml'
     */
    protected $strategy;

    protected $exclusionCategories = array();

    /**
     * @var integer
     * ttl
     */
    protected $ttl;

    public function __construct($name, $url, $category, $strategy, $ttl)
    {
        $this->name = $name;
        $this->url = $url;
        $this->category = $category;
        $this->strategy = $strategy;
        $this->ttl = $ttl;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return string
     */
    public function getStrategy()
    {
        return $this->strategy;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    public function getTtl()
    {
        return $this->ttl;
    }

    public function isCacheEnabled()
    {
        return $this->ttl > 0;
    }

    public function addExclusionCategory($name)
    {
        $this->exclusionCategories[] = $name;
    }

    public function getExclusionCategories()
    {
        return $this->exclusionCategories;
    }
} 
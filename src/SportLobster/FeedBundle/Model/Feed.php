<?php

namespace SportLobster\FeedBundle\Model;

use DateTime;

use JMS\Serializer\Annotation as JMS;

class Feed
{
    /**
     * @var  string
     * @JMS\Type("string")
     */
    protected $title;

    /**
     * @var  string
     * @JMS\Type("string")
     */
    protected $description;

    /**
     * @var  string
     * @JMS\Type("string")
     */
    protected $link;

    /**
     * @var DateTime
     * @JMS\Accessor(setter="convertDate")
     * @JMS\Type("string")
     * @JMS\SerializedName("pubDate")
     */
    protected $date;

    /**
     * @var  string
     * @JMS\Type("string")
     */
    protected $category;

    /**
     * @var Enclosure
     * @JMS\Accessor(setter="setImage")
     * @JMS\Type("SportLobster\FeedBundle\Model\Enclosure")
     * @JMS\SerializedName("enclosure")
     */
    protected $image;

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Tue, 24 Jun 2014 12:58:17 GMT
     * YYYY-MM-DD HH:MM)
     * @param $date
     */
    public function convertDate($date)
    {
        $this->date = new \DateTime($date);
    }

    /**
     * @param \DateTime $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return \DateTime
     */
    public function getImage()
    {
        return $this->image->getUrl();
    }

    public function isReport()
    {
        return $this->category == 'Report';
    }
} 
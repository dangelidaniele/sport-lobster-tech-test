<?php
/**
 * Created by PhpStorm.
 * User: foodity
 * Date: 27/07/14
 * Time: 11:26
 */

namespace SportLobster\FeedBundle\Model;
use JMS\Serializer\Annotation as JMS;

class Rss
{
    /**
     * @JMS\Type("SportLobster\FeedBundle\Model\Channel")
     */
    protected $channel;

    /**
     * @param mixed $channel
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;
    }

    /**
     * @return mixed
     */
    public function getChannel()
    {
        return $this->channel;
    }
} 
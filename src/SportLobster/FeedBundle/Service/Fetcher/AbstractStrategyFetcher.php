<?php


namespace SportLobster\FeedBundle\Service\Fetcher;


use Guzzle\Http\ClientInterface;
use Guzzle\Service\Client;
use SportLobster\FeedBundle\Model\ChannelInfo;

abstract class AbstractStrategyFetcher
{
    /**
     * @var ChannelInfo
     */
    protected $channelInfo;

    /**
     * @var Client
     */
    protected $client;


    public function __construct(ChannelInfo $channelInfo, ClientInterface $client)
    {
        $this->channelInfo = $channelInfo;
        $this->client = $client;
    }
} 
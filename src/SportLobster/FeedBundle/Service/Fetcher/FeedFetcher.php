<?php

namespace SportLobster\FeedBundle\Service\Fetcher;

use Guzzle\Service\ClientInterface;
use SportLobster\FeedBundle\Exception\StrategyNotFoundException;
use SportLobster\FeedBundle\Model\ChannelInfo;

class FeedFetcher
{
    /** @var \Guzzle\Service\Client  */
    protected  $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }
    /**
     * @param \SportLobster\FeedBundle\Model\ChannelInfo $channelInfo
     * @throws \SportLobster\FeedBundle\Exception\StrategyNotFoundException
     * @internal param \Guzzle\Service\Client $client
     * @internal param $url
     * @internal param $strategy
     * @return null|JsonStrategyFetcher
     */
    public function getFetcher(ChannelInfo $channelInfo)
    {
        $fetcher = null;
        $strategy = $channelInfo->getStrategy();
        switch($strategy) {
            case 'json': return new JsonStrategyFetcher($channelInfo, $this->client);
                break;
            case 'xml': return new XmlStrategyFetcher($channelInfo, $this->client);
                break;
        }

        throw new StrategyNotFoundException(sprintf('The strategy %s is not supported', $strategy));
    }

}
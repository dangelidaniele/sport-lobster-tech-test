<?php

namespace SportLobster\FeedBundle\Service\Fetcher;


interface FetcherStrategyInterface
{
    /**
     * @return string
     */
    public function fetch();
} 
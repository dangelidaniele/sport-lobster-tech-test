<?php

namespace SportLobster\FeedBundle\Service\Fetcher;


use Guzzle\Service\Client;
use Guzzle\Service\ClientInterface;
use SportLobster\FeedBundle\Model\ChannelInfo;

class JsonStrategyFetcher extends AbstractStrategyFetcher implements FetcherStrategyInterface
{
    public function __construct(ChannelInfo $channelInfo, ClientInterface $client)
    {
        parent::__construct($channelInfo, $client);
    }

    /**
     * @return \Guzzle\Http\EntityBodyInterface|string
     */
    public function fetch()
    {
        // TODO: Implement fetch() method.
    }
}
<?php

namespace SportLobster\FeedBundle\Service\Fetcher;


use Guzzle\Service\ClientInterface;
use SportLobster\FeedBundle\Model\ChannelInfo;

class XmlStrategyFetcher extends AbstractStrategyFetcher implements FetcherStrategyInterface
{


    public function __construct(ChannelInfo $channelInfo, ClientInterface $client)
    {
        parent::__construct($channelInfo, $client);
    }

    /**
     * @return \Guzzle\Http\EntityBodyInterface|string
     */
    public function fetch()
    {
        $request = $this->client->get($this->channelInfo->getUrl());

        if($this->channelInfo->isCacheEnabled()) {
            $request->getParams()->set('cache.override_ttl', $this->channelInfo->getTtl());
        }

        $data = $request->send()->getBody(true);

        return $data;
    }
}
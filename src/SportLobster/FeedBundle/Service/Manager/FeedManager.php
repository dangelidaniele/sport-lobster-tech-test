<?php

namespace SportLobster\FeedBundle\Service\Manager;


use JMS\Serializer\Serializer;
use SportLobster\FeedBundle\Exception\ParsingException;
use SportLobster\FeedBundle\Model\Channel;
use SportLobster\FeedBundle\Model\ChannelInfo;
use SportLobster\FeedBundle\Service\Fetcher\FeedFetcher;

class FeedManager implements FeedManagerInterface
{
    /**
     * @var FeedFetcher
     */
    protected $fetcher;

    protected $exclusionCategories = array('Preview');

    /**
     * @var Serializer
     */
    protected $serializer;

    public function __construct(FeedFetcher $fetcher, Serializer $serializer)
    {
        $this->fetcher = $fetcher;
        $this->serializer = $serializer;
    }

    /**
     * @param ChannelInfo $channelInfo
     * @throws \SportLobster\FeedBundle\Exception\ParsingException
     * @return Channel
     */
    public function getFeedsByChannel(ChannelInfo $channelInfo)
    {
        $fetcher = $this->fetcher->getFetcher($channelInfo);

        try {
            $data = $fetcher->fetch();
            $rssFeed = $this->serializer->deserialize(
                $data,
                'SportLobster\FeedBundle\Model\Rss',
                $channelInfo->getStrategy()
            );

            /** @var Channel $channel */
            $channel = $rssFeed->getChannel();

            foreach ($channel->getFeeds() as $feed) {
                if (in_array($feed->getCategory(), $channelInfo->getExclusionCategories())) {
                    $channel->removeFeed($feed);
                }
            }

            return $channel;

        } catch (\Exception $e) {
            throw new ParsingException($e->getMessage());
        }
    }

    /**
     * @param \SportLobster\FeedBundle\Model\ChannelInfo $channelInfo
     * @param $categoryName
     * @throws \SportLobster\FeedBundle\Exception\ParsingException
     * @internal param $channelName
     * @return mixed
     */
    public function getFeedsChannelByCategory(ChannelInfo $channelInfo, $categoryName)
    {
        $fetcher = $this->fetcher->getFetcher($channelInfo);
        try {

            $data = $fetcher->fetch();

            $rssFeed = $this->serializer->deserialize(
                $data,
                'SportLobster\FeedBundle\Model\Rss',
                $channelInfo->getStrategy()
            );
            /** @var Channel $channel */
            $channel = $rssFeed->getChannel();

            foreach ($channel->getFeeds() as $feed) {
                if ($feed->getCategory() != $categoryName) {
                    $channel->removeFeed($feed);
                }
            }

            return $channel;

        } catch (\Exception $e) {
            throw new ParsingException($e->getMessage());
        }
    }
}
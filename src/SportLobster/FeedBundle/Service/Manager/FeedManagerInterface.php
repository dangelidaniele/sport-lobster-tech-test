<?php

namespace SportLobster\FeedBundle\Service\Manager;


use SportLobster\FeedBundle\Model\Channel;
use SportLobster\FeedBundle\Model\ChannelInfo;

interface FeedManagerInterface
{
    /**
     * @param \SportLobster\FeedBundle\Model\ChannelInfo $channelInfo
     * @internal param $channelName
     * @return Channel
     */
    public function getFeedsByChannel(ChannelInfo $channelInfo);

    /**
     * @param \SportLobster\FeedBundle\Model\ChannelInfo $channelInfo
     * @param $categoryName
     * @internal param $channelName
     * @return Channel
     */
    public function getFeedsChannelByCategory(ChannelInfo $channelInfo, $categoryName);

}
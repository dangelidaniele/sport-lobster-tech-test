<?php

namespace SportLobster\FeedBundle\Service\Manager;


use SportLobster\FeedBundle\Exception\SourceChannelNotFoundException;
use SportLobster\FeedBundle\Model\ChannelInfo;

class SourceFeedManager implements SourceFeedManagerInterface
{
    /**
     * @var ChannelInfo[]
     */
    protected $sourceChannelInfo;

    public function __construct($config)
    {
        $channelSources = $config['channels_sources'];

        foreach ($channelSources as $name => $config) {

            $channelInfo = new ChannelInfo(
                $name,
                $config['url'],
                $config['category'],
                $config['strategy'],
                $config['ttl']
            );
            if(isset($config['exclusion_categories'])) {
                foreach ($config['exclusion_categories'] as $exclusionCategory) {
                    $channelInfo->addExclusionCategory($exclusionCategory);
                }
            }

            $this->sourceChannelInfo[$name] = $channelInfo;
        }
    }

    /**
     * @param $name
     * @throws \SportLobster\FeedBundle\Exception\SourceChannelNotFoundException
     * @return ChannelInfo
     */
    public function findSourceByName($name)
    {
        if (!isset($this->sourceChannelInfo[$name])) {
            throw new SourceChannelNotFoundException;
        }
        return $this->sourceChannelInfo[$name];
    }
}
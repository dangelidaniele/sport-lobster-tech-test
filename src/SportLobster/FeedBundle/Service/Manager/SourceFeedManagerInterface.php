<?php

namespace SportLobster\FeedBundle\Service\Manager;

use SportLobster\FeedBundle\Model\ChannelInfo;

interface SourceFeedManagerInterface
{
    /**
     * @param $name
     * @throws \SportLobster\FeedBundle\Exception\SourceChannelNotFoundException
     * @return ChannelInfo
     */
    public function findSourceByName($name);

} 
<?php

namespace SportLobster\FeedBundle\Tests\Service\Fetcher;

use Guzzle\Service\Client;
use SportLobster\FeedBundle\Service\Fetcher\FeedFetcher;

class FeedFetcherTest extends \PHPUnit_Framework_TestCase
{
    /** @var  FeedFetcher */
    protected $feedFetcher;

    public function setUp()
    {
        $client = $this->getMockBuilder('Guzzle\Service\ClientInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $this->feedFetcher = new FeedFetcher($client);
    }

    public function testGetXmlStrategyFetcher()
    {
        $channelInfo = $this->getMockBuilder('SportLobster\FeedBundle\Model\ChannelInfo')
            ->disableOriginalConstructor()
            ->getMock();

        $channelInfo->expects($this->once())
            ->method('getStrategy')
            ->will($this->returnValue('xml'));

        $strategy = $this->feedFetcher->getFetcher($channelInfo);

        $this->assertInstanceOf('SportLobster\FeedBundle\Service\Fetcher\XmlStrategyFetcher', $strategy);
    }

    public function testGetJsonStrategyFetcher()
    {
        $channelInfo = $this->getMockBuilder('SportLobster\FeedBundle\Model\ChannelInfo')
            ->disableOriginalConstructor()
            ->getMock();

        $channelInfo->expects($this->once())
            ->method('getStrategy')
            ->will($this->returnValue('json'));

        $strategy = $this->feedFetcher->getFetcher($channelInfo);

        $this->assertInstanceOf('SportLobster\FeedBundle\Service\Fetcher\JsonStrategyFetcher', $strategy);
    }

    /**
     * @expectedException \SportLobster\FeedBundle\Exception\StrategyNotFoundException
     */
    public function testExceptionStrategyFetcher()
    {
        $channelInfo = $this->getMockBuilder('SportLobster\FeedBundle\Model\ChannelInfo')
            ->disableOriginalConstructor()
            ->getMock();

        $channelInfo->expects($this->once())
            ->method('getStrategy')
            ->will($this->returnValue('??????'));

        $this->feedFetcher->getFetcher($channelInfo);

    }

} 
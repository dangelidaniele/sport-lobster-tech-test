<?php

namespace SportLobster\FeedBundle\Tests\Service\Manager;


use Doctrine\Common\Collections\ArrayCollection;
use SportLobster\FeedBundle\Service\Manager\FeedManager;
use SportLobster\FeedBundle\Service\Manager\FeedManagerInterface;

class FeedManagerTest extends \PHPUnit_Framework_TestCase
{
    /** @var  FeedManagerInterface */
    protected $feedManager;

    /** @var  \PHPUnit_Framework_MockObject_MockObject*/
    protected $serializer;

    /** @var  \PHPUnit_Framework_MockObject_MockObject*/
    protected $feedFetcher;

    public function setUp()
    {
        $this->feedFetcher = $this->getMockBuilder('SportLobster\FeedBundle\Service\Fetcher\FeedFetcher')
             ->disableOriginalConstructor()
            ->getMock();

        $this->serializer = $this->getMockBuilder('JMS\Serializer\Serializer')
            ->disableOriginalConstructor()
            ->getMock();

        $this->feedManager = new FeedManager($this->feedFetcher, $this->serializer);
    }

    public function testGetFeedsByChannel()
    {
        $channelInfo = $this->getMockBuilder('SportLobster\FeedBundle\Model\ChannelInfo')
            ->disableOriginalConstructor()
            ->getMock();

        $channelInfo->expects($this->exactly(2))
            ->method('getExclusionCategories')
            ->will($this->returnValue(array('Preview')));

        $channelInfo->expects($this->once())
            ->method('getStrategy')
            ->will($this->returnValue('xml'));

        $strategyFetcherMock = $this->getMockBuilder('SportLobster\FeedBundle\Service\Fetcher\FetcherStrategyInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $this->feedFetcher->expects($this->once())
            ->method('getFetcher')
            ->with($this->equalTo($channelInfo))
            ->will($this->returnValue($strategyFetcherMock));

        $strategyFetcherMock->expects($this->once())
            ->method('fetch')
            ->will($this->returnValue('response data'));

        $rssMock = $this->getMockBuilder('SportLobster\FeedBundle\Model\Rss')
            ->disableOriginalConstructor()
            ->getMock();

        $channelsMock = $this->getChannelsForTestByChannel();

        $rssMock->expects($this->once())
            ->method('getChannel')
            ->will($this->returnValue($channelsMock));

        $this->serializer->expects($this->once())
            ->method('deserialize')
            ->with($this->equalTo('response data'), $this->equalTo('SportLobster\FeedBundle\Model\Rss'),$this->equalTo('xml'))
            ->will($this->returnValue($rssMock));

        $this->feedManager->getFeedsByChannel($channelInfo);
    }

    public function testGetFeedsChannelByCategory()
    {
        $channelInfo = $this->getMockBuilder('SportLobster\FeedBundle\Model\ChannelInfo')
            ->disableOriginalConstructor()
            ->getMock();

        $channelInfo->expects($this->once())
            ->method('getStrategy')
            ->will($this->returnValue('xml'));

        $strategyFetcherMock = $this->getMockBuilder('SportLobster\FeedBundle\Service\Fetcher\FetcherStrategyInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $this->feedFetcher->expects($this->once())
            ->method('getFetcher')
            ->with($this->equalTo($channelInfo))
            ->will($this->returnValue($strategyFetcherMock));

        $strategyFetcherMock->expects($this->once())
            ->method('fetch')
            ->will($this->returnValue('response data'));

        $rssMock = $this->getMockBuilder('SportLobster\FeedBundle\Model\Rss')
            ->disableOriginalConstructor()
            ->getMock();

        $channelsMock = $this->getChannelsForTestByCategory();

        $rssMock->expects($this->once())
            ->method('getChannel')
            ->will($this->returnValue($channelsMock));

        $this->serializer->expects($this->once())
            ->method('deserialize')
            ->with($this->equalTo('response data'), $this->equalTo('SportLobster\FeedBundle\Model\Rss'),$this->equalTo('xml'))
            ->will($this->returnValue($rssMock));


        $this->feedManager->getFeedsChannelByCategory($channelInfo, 'Report');

    }

    private function getChannelsForTestByCategory()
    {
        $channelMock = $this->getMockBuilder('SportLobster\FeedBundle\Model\Channel')
            ->disableOriginalConstructor()
            ->getMock();

        $feedOne = $this->getMockBuilder('SportLobster\FeedBundle\Model\Feed')
            ->disableOriginalConstructor()
            ->getMock();

        $feedOne->expects($this->once())
            ->method('getCategory')
            ->will($this->returnValue('Report'));

        $feedTwo = $this->getMockBuilder('SportLobster\FeedBundle\Model\Feed')
            ->disableOriginalConstructor()
            ->getMock();

        $feedTwo->expects($this->once())
            ->method('getCategory')
            ->will($this->returnValue('News'));

        $channelMock->expects($this->once())
            ->method('removeFeed')
             ->with($this->equalTo($feedTwo));

        $channelMock->expects($this->once())
            ->method('getFeeds')
            ->will($this->returnValue(new ArrayCollection(array($feedOne, $feedTwo))));

        return $channelMock;
    }

    private function getChannelsForTestByChannel()
    {
        $channelMock = $this->getMockBuilder('SportLobster\FeedBundle\Model\Channel')
            ->disableOriginalConstructor()
            ->getMock();

        $feedOne = $this->getMockBuilder('SportLobster\FeedBundle\Model\Feed')
            ->disableOriginalConstructor()
            ->getMock();

        $feedOne->expects($this->once())
            ->method('getCategory')
            ->will($this->returnValue('Report'));

        $feedTwo = $this->getMockBuilder('SportLobster\FeedBundle\Model\Feed')
            ->disableOriginalConstructor()
            ->getMock();

        $feedTwo->expects($this->once())
            ->method('getCategory')
            ->will($this->returnValue('Preview'));

        $channelMock->expects($this->once())
            ->method('removeFeed')
            ->with($this->equalTo($feedTwo));

        $channelMock->expects($this->once())
            ->method('getFeeds')
            ->will($this->returnValue(new ArrayCollection(array($feedOne, $feedTwo))));

        return $channelMock;
    }

} 
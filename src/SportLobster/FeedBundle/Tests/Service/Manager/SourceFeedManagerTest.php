<?php

namespace SportLobster\FeedBundle\Tests\Service\Manager;

use SportLobster\FeedBundle\Service\Manager\SourceFeedManager;

class SourceFeedManagerTest extends \PHPUnit_Framework_TestCase
{
    /** @var  SourceFeedManager */
    protected $sourceFeedManager;

    public function setUp()
    {
        $config = array(
            'channels_sources' => array(
                'sport_sky' =>
                    array(
                        'url' => 'www.sky.com',
                        'category' => 'football',
                        'strategy' => 'json',
                        'ttl' => 120
                    )
            )
        );

        $this->sourceFeedManager = new SourceFeedManager($config);
    }

    public function testFindSourceByName()
    {
        $channelInfo = $this->sourceFeedManager->findSourceByName('sport_sky');
        $this->assertEquals(120, $channelInfo->getTtl());
        $this->assertEquals('www.sky.com', $channelInfo->getUrl());
        $this->assertEquals('football', $channelInfo->getCategory());
        $this->assertEquals('json', $channelInfo->getStrategy());
    }

    /**
     * @expectedException \SportLobster\FeedBundle\Exception\SourceChannelNotFoundException
     */
    public function testFindSourceByNameException()
    {
        $this->sourceFeedManager->findSourceByName('mmmm');
    }
} 
<?php

namespace SportLobster\TestCoreBundle\Controller;


use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SportLobster\FeedBundle\Exception;

class NewsFeedController extends Controller
{
    /**
     * News Action
     */
    public function footballAction()
    {
        $channel = null;
        $error = null;

        try {
            $channel = $this->container->get('sport_lobster.football_news.service')
                ->getFootballNews();

            return $this->render('SportLobsterTestCoreBundle:NewsFeed:football.html.twig',
                array(
                    'channel' => $channel,
                    'feeds' => $channel->getFeeds(),
                    'error' => $error
                )
            );

        } catch(Exception $e) {
            $error = $e->getMessage();
        }
        return $this->render('SportLobsterTestCoreBundle:NewsFeed:error.html.twig',
            array(
                'error' => $error,
            )
        );
    }

    public function footballCategoryAction($category)
    {
        $channel = null;
        $error = null;

        try {
            $channel = $this->container->get('sport_lobster.football_news.service')
                ->getFootballNewsFilteredByCategory($category);

            return $this->render('SportLobsterTestCoreBundle:NewsFeed:football.html.twig',
                array(
                    'channel' => $channel,
                    'feeds' => $channel->getFeeds(),
                    'error' => $error
                )
            );

        } catch(Exception $e) {
            $error = $e->getMessage();
        }

        return $this->render('SportLobsterTestCoreBundle:NewsFeed:error.html.twig',
            array(
                'error' => $error,
            )
        );
    }


} 
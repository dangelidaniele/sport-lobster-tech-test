<?php

namespace SportLobster\TestCoreBundle\Service;

use SportLobster\FeedBundle\Service\Manager\FeedManagerInterface;
use SportLobster\FeedBundle\Service\Manager\SourceFeedManagerInterface;

class FootballNewsService
{

    /** @var  SourceFeedManagerInterface */
    protected $sourceFeedManager;

    /** @var FeedManagerInterface */
    protected $feedManager;

    public function __construct(SourceFeedManagerInterface $sourceFeedManager, FeedManagerInterface $feedManager)
    {
        $this->sourceFeedManager = $sourceFeedManager;
        $this->feedManager = $feedManager;
    }


    /**
     * @return \SportLobster\FeedBundle\Model\Channel
     * Get the sky_sport_football channel info
     * The name is hardcoded and can be retrieved from a manager that will
     * return a list if channels than can be used to fetching the data.
     *
     * For this test I just used this simple work around
     */
    public function getFootballNews()
    {
        $channelInfo = $this->sourceFeedManager
            ->findSourceByName('sky_sport_football');

        return $this->feedManager->getFeedsByChannel($channelInfo);
    }

    /**
     * @param $category
     * @return \SportLobster\FeedBundle\Model\Channel
     */
    public function getFootballNewsFilteredByCategory($category)
    {
        $channelInfo = $this->sourceFeedManager
            ->findSourceByName('sky_sport_football');

        return $this->feedManager->getFeedsChannelByCategory($channelInfo, $category);
    }

}
<?php

namespace SportLobster\TestCoreBundle\Tests\Controller;


use Guzzle\Http\Message\Response;
use Guzzle\Plugin\Mock\MockPlugin;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class NewsFeedControllerTest extends WebTestCase
{
    public function testFootball()
    {
        $client = static::createClient();
        /** inject a mock of guzzle client */
        $client->getContainer()->set('sport_lobster.guzzle_client', $this->prepareGuzzleClient());

        $crawler = $client->request('GET', '/football');
        //2 feed because in the feed.xml there is one category Preview ( that is in the exclusion categories )
        $this->assertEquals(2,$crawler->filter('.feed')->count());

    }

    public function testFootballReport()
    {
        $client = static::createClient();
        /** inject a mock of guzzle client */
        $client->getContainer()->set('sport_lobster.guzzle_client', $this->prepareGuzzleClient());

        $crawler = $client->request('GET', '/football/Report');
        $this->assertEquals(1, $crawler->filter('.feed')->count());

    }

    public function testNoResultFootball()
    {
        $client = static::createClient();
        $client->getContainer()->set('sport_lobster.guzzle_client', $this->prepareGuzzleClientWithNoResult());

        $crawler = $client->request('GET', '/football');
        $this->assertEquals(0, $crawler->filter('.feed')->count());
        $this->assertEquals('There aren\'t feed',  $crawler->filter('.nofeed')->text());
    }

    public function testMalformedFeed()
    {
        $client = static::createClient();
        $client->getContainer()->set('sport_lobster.guzzle_client', $this->prepareGuzzleClientWithMalformed());

        $crawler = $client->request('GET', '/football');
        $this->assertEquals(0, $crawler->filter('.feed')->count());
        $this->assertEquals(1, $crawler->filter('.error')->count());
    }

    /**
     * Mocking guzzle client with Mock plugin
     * @return \Guzzle\Service\Client
     */
    private function prepareGuzzleClient()
    {
        $plugin = new MockPlugin();

        $response = new Response(200);
        $response->setBody(file_get_contents(__DIR__.'/../Stubs/feed.xml'));
        $plugin = $plugin->addResponse($response);

        $clientHttp = new \Guzzle\Service\Client();
        $clientHttp->addSubscriber($plugin);

        return $clientHttp;
    }

    private function prepareGuzzleClientWithNoResult()
    {
        $plugin = new MockPlugin();

        $response = new Response(200);
        $response->setBody(file_get_contents(__DIR__.'/../Stubs/feed_no_result.xml'));
        $plugin = $plugin->addResponse($response);

        $clientHttp = new \Guzzle\Service\Client();
        $clientHttp->addSubscriber($plugin);

        return $clientHttp;
    }

    private function prepareGuzzleClientWithMalformed()
    {
        $plugin = new MockPlugin();

        $response = new Response(200);
        $response->setBody(file_get_contents(__DIR__.'/../Stubs/feed_malformed.xml'));
        $plugin = $plugin->addResponse($response);

        $clientHttp = new \Guzzle\Service\Client();
        $clientHttp->addSubscriber($plugin);

        return $clientHttp;
    }

} 